    --Département
INSERT INTO departement
(id, nom)
VALUES(1, 'Direction et Administration Générale');
INSERT INTO departement
(id, nom)
VALUES(2, 'Achat');
INSERT INTO departement
(id, nom)
VALUES(3, 'Finance et Compatbilité');
INSERT INTO departement
(id, nom)
VALUES(4, 'Logistique');
INSERT INTO departement
(id, nom)
VALUES(5, 'Marketing et Commerciale');
INSERT INTO departement
(id, nom)
VALUES(6, 'Production');

    --Employé
INSERT INTO employe
(id, nom, iddepartement)
VALUES(1, 'Etienne', 1);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(2, 'Sebastien', 1);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(3, 'Benjamin', 1);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(4, 'Sonia', 2);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(5, 'Tafita', 2);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(6, 'Mihoby', 2);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(7, 'Maphie', 3);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(8, 'Mic', 3);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(9, 'Reynolds', 3);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(10, 'Ando', 4);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(11, 'Heriniavo', 4);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(12, 'Anja', 4);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(13, 'Valisoa', 5);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(14, 'Rova', 5);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(15, 'Andrianina', 5);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(16, 'Anna', 6);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(17, 'Volana', 6);
INSERT INTO employe
(id, nom, iddepartement)
VALUES(18, 'Denis', 6);
