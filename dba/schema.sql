CREATE TABLE departement(
    id INTEGER PRIMARY KEY,
    nom VARCHAR(100)
);

CREATE TABLE employe(
    id INTEGER PRIMARY KEY,
    nom VARCHAR(100),
    idDepartement INTEGER REFERENCES departement(id)
);