package controller;
import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.*;
import servlet.FrontServlet;

public class TestServlet extends FrontServlet{
    public void init()throws ServletException{
        super.init();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        processRequest(request, response);
    }
    protected void processRequest(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException {
        super.processRequest(req, res);
    }
}
