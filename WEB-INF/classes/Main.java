import controller.TestServlet;
import obj.Departement;
import obj.Employe;

public class Main {
    public static void main(String[]args) throws Exception {
        new TestServlet();
        new Departement();
        new Employe();
    }
}
