package obj;
import annotation.URL;
import dao.GenericDAO;

@URL(value = "Departement")
public class Departement extends GenericDAO {
    int id;
    String nom;

    public Departement() {}

    public Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

}
