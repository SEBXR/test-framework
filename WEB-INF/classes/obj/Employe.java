package obj;
import annotation.URL;
import dao.GenericDAO;

@URL(value = "Employe")
public class Employe extends GenericDAO{
    int id;
    String nom;
    int idDepartement;
    

    public Employe() {}

    public Employe(int id, String nom, int idDepartement) {
        this.id = id;
        this.nom = nom;
        this.idDepartement = idDepartement;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdDepartement() {
        return this.idDepartement;
    }

    public void setIdDepartement(int idDepartement) {
        this.idDepartement = idDepartement;
    }
}
