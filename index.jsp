<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ page import='obj.*'%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Index Akia</title>
    </head>
    <body>
        <h1>Lister :</h1>
        <ul>
            <li><a href="Departement/select/.do">Département</a></li>
            <li><a href="Employe/select/.do">Employé</a></li>
        </ul>
        <h1>Insérer:</h1>
        <ul>
            <li>
                <h2>Département</h2>
                <%= new Departement().getFormulaire("insert")%>
            </li>
            <li>
                <h2>Employe</h2>
                <%= new Employe().getFormulaire("insert")%>
            </li>
        </ul>
    </body>
</html>