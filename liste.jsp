<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ page import="obj.* , java.util.HashMap , util.* , java.util.ArrayList , java.lang.reflect.*"%>
<%
    HashMap <String,ArrayList<Object>> data = (HashMap <String,ArrayList<Object>>)request.getAttribute("data");
    ArrayList<Object> list = data.get("data");
    Class cl = list.get(0).getClass();
    Object obj = Class.forName(cl.getName()).newInstance();
    Method [] methods = cl.getDeclaredMethods();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Liste</title>
    </head>
    <body>
        <h1>La liste des <%= cl.getSimpleName()%> : </h1>
        <table border="1">
            <tr>
            <%
                for(Method meth : methods) {
                    String methodName = meth.getName();
                    if(methodName.startsWith("get")){
                        String titre = methodName.replace("get","");
                        out.print("<td>"+titre+"</td>");
                    }
                }
            %>
            </tr>
            <%
                for(Object object : list){
                    out.print("<tr>");
                    for(Method method : methods){
                        String methodName = method.getName();
                        if(methodName.startsWith("get")){
                            out.print("<td>"+method.invoke(object)+"</td>");
                        }
                    }
                    out.print("</tr>");
                }   
            %>
        </table>
        <a href="/test-framework/index.jsp">Retour</a>
    </body>
</html>